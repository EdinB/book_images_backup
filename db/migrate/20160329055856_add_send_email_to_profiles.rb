class AddSendEmailToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :send_email, :boolean, default: true
  end
end
