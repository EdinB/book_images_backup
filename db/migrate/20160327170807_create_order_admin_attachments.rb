class CreateOrderAdminAttachments < ActiveRecord::Migration
  def change
    create_table :order_admin_attachments do |t|
      t.integer :order_id
      t.string :attachment

      t.timestamps null: false
    end
  end
end
