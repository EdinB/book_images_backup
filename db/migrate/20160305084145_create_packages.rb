class CreatePackages < ActiveRecord::Migration
  def change
    create_table :packages do |t|
      t.belongs_to :group, index: true, foreign_key: true
      t.string :name
      t.float :price

      t.timestamps null: false
    end
  end
end
