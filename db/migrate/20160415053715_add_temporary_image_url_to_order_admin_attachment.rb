class AddTemporaryImageUrlToOrderAdminAttachment < ActiveRecord::Migration
  def change
    add_column :order_admin_attachments, :temporary_image_url, :string
  end
end
