class AddAttachmentProcessingToOrderAdminAttachments < ActiveRecord::Migration
  def change
    add_column :order_admin_attachments, :attachment_processing, :boolean, null: false, default: false
  end
end
