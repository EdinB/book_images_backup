class CreateGroupPackages < ActiveRecord::Migration
  def change
    create_table :group_packages do |t|
      t.belongs_to :group, index: true, foreign_key: true
      t.belongs_to :package, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
