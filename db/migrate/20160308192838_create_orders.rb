class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.string :name
      t.string :phone
      t.string :phone2
      t.string :address
      t.string :city
      t.string :case_number
      t.text :comments

      t.timestamps null: false
    end
  end
end
