class RemoveGroupIdFromPackages < ActiveRecord::Migration
  def change
    remove_column :packages, :group_id, :integer
  end
end
