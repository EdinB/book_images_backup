class AddExtraFieldsForOrders < ActiveRecord::Migration
  def change
    add_column :orders, :kind, :string
    add_column :orders, :email, :string
    add_column :orders, :square_meters, :string
    add_column :orders, :total_levels, :string
    add_column :orders, :delivery_time, :datetime
  end
end
