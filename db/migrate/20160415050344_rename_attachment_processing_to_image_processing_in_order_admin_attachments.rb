class RenameAttachmentProcessingToImageProcessingInOrderAdminAttachments < ActiveRecord::Migration
  def change
    rename_column :order_admin_attachments, :attachment_processing, :image_processing
  end
end
