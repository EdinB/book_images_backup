class AddAttachmentsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :attachments, :json
  end
end
