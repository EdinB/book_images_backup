class AddAdminAttachmentsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :admin_attachments, :json
  end
end
