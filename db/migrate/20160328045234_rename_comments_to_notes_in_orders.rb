class RenameCommentsToNotesInOrders < ActiveRecord::Migration
  def change
    rename_column :orders, :comments, :notes
  end
end
