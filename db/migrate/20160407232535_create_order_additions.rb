class CreateOrderAdditions < ActiveRecord::Migration
  def change
    create_table :order_additions do |t|
      t.belongs_to :order, index: true, foreign_key: true
      t.text :note
      t.integer :price
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
