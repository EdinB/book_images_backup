class AddAllowUploadToPackages < ActiveRecord::Migration
  def change
    add_column :packages, :allow_upload, :boolean
  end
end
