class AddMaxSquareMetersToPackages < ActiveRecord::Migration
  def change
    add_column :packages, :max_80_square_meters, :boolean
  end
end
