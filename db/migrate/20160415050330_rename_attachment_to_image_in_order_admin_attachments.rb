class RenameAttachmentToImageInOrderAdminAttachments < ActiveRecord::Migration
  def change
    rename_column :order_admin_attachments, :attachment, :image
  end
end
