class CreatePackageExtras < ActiveRecord::Migration
  def change
    create_table :package_extras do |t|
      t.belongs_to :package, index: true, foreign_key: true
      t.belongs_to :extra, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
