class ChangeSquareMeterstoInteger < ActiveRecord::Migration
  def change
    change_column :orders, :square_meters, 'integer USING CAST(square_meters AS integer)'
  end
end
