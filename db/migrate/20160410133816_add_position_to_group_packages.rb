class AddPositionToGroupPackages < ActiveRecord::Migration
  def change
    add_column :group_packages, :position, :integer
  end
end
