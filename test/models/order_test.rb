# == Schema Information
#
# Table name: orders
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  name                :string
#  phone               :string
#  phone2              :string
#  address             :string
#  city                :string
#  case_number         :string
#  notes               :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  state               :string
#  kind                :string
#  email               :string
#  square_meters       :integer
#  total_levels        :string
#  delivery_time       :datetime
#  attachments         :json
#  admin_attachments   :json
#  temporary_image_url :string
#
# Indexes
#
#  index_orders_on_user_id  (user_id)
#

require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
