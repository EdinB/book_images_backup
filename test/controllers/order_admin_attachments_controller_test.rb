require 'test_helper'

class OrderAdminAttachmentsControllerTest < ActionController::TestCase
  setup do
    @order_admin_attachment = order_admin_attachments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:order_admin_attachments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create order_admin_attachment" do
    assert_difference('OrderAdminAttachment.count') do
      post :create, order_admin_attachment: { attachment: @order_admin_attachment.attachment, order_id: @order_admin_attachment.order_id }
    end

    assert_redirected_to order_admin_attachment_path(assigns(:order_admin_attachment))
  end

  test "should show order_admin_attachment" do
    get :show, id: @order_admin_attachment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @order_admin_attachment
    assert_response :success
  end

  test "should update order_admin_attachment" do
    patch :update, id: @order_admin_attachment, order_admin_attachment: { attachment: @order_admin_attachment.attachment, order_id: @order_admin_attachment.order_id }
    assert_redirected_to order_admin_attachment_path(assigns(:order_admin_attachment))
  end

  test "should destroy order_admin_attachment" do
    assert_difference('OrderAdminAttachment.count', -1) do
      delete :destroy, id: @order_admin_attachment
    end

    assert_redirected_to order_admin_attachments_path
  end
end
