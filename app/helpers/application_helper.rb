module ApplicationHelper
  include CurrencyHelper
  BOOTSTRAP_FLASH_MSG = {
    success: 'alert-success',
    error: 'alert-danger',
    alert: 'alert-danger',
    notice: 'alert-info'
  }

  def bootstrap_class_for(flash_type)
    BOOTSTRAP_FLASH_MSG.fetch(flash_type, flash_type.to_s)
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type.to_sym)}", role: "alert") do 
        concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
        concat message 
      end)
    end
    nil
  end

  def sortable(column, title = nil, type = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction == "asc" ? 'fa fa-caret-down' : 'fa fa-caret-up' }" : "fa fa-unsorted"
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    case type
      when "Package"
        sortable_url = packages_path(sort: column, direction: direction)
      when "Order"
        sortable_url = root_path(sort: column, direction: direction, state: params[:state])
      when "Search"
        sortable_url = search_orders_path(sort: column, direction: direction, q: params[:q])
      else
        sortable_url = {:sort => column, :direction => direction}
    end
    link_to title, sortable_url, {class: css_class}
  end
end
