# == Schema Information
#
# Table name: profiles
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  first_name :string
#  last_name  :string
#  shop       :string
#  shop_no    :string
#  address    :text
#  telephone  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module ProfilesHelper
end
