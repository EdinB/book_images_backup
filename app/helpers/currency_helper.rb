module CurrencyHelper
  def danish_currency number
    number_to_currency(number, locale: :da, precision: 2,  unit: "DKK", separator: ",", delimiter: ".", format: "%n <small class='currency-unit'>%u</small>".html_safe)
  end
end