# == Schema Information
#
# Table name: orders
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  name        :string
#  phone       :string
#  phone2      :string
#  address     :string
#  city        :string
#  case_number :string
#  comments    :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

module OrdersHelper
end
