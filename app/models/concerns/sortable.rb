module Sortable
  extend ActiveSupport::Concern

  module ClassMethods
    def custom_sort(sort_column, sort_direction, type = "default")
      results = self.where(nil)
      if sort_column.present? and sort_direction.present?
        unless type == "search"
          results = results.public_send("default_sort", sort_column, sort_direction)
        else
          results = results.public_send("search_sort", sort_column, sort_direction)
        end
      end
      results
    end
  end
end