# == Schema Information
#
# Table name: packages
#
#  id                   :integer          not null, primary key
#  name                 :string
#  price                :float
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  description          :text
#  allow_upload         :boolean
#  max_80_square_meters :boolean
#

class Package < ActiveRecord::Base
  include Sortable
  has_many :group_packages, dependent: :destroy
  has_many :groups, through: :group_packages

  has_many :package_extras, dependent: :destroy
  has_many :extras, through: :package_extras

  accepts_nested_attributes_for :package_extras, allow_destroy: true

  validates_presence_of :name, :price

  scope :default_sort, -> (column, direction) {order(column + " " + direction)}

  def to_s
    self.name
  end
end
