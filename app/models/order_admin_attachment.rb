# == Schema Information
#
# Table name: order_admin_attachments
#
#  id                  :integer          not null, primary key
#  order_id            :integer
#  image               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  image_processing    :boolean          default("false"), not null
#  temporary_image_url :string
#

class OrderAdminAttachment < ActiveRecord::Base
  mount_uploader :image, AdminAttachmentUploader
  # process_in_background :image
  belongs_to :order

  validates :temporary_image_url, presence: true
  
  # attr_accessible :image, :user_id, :original_image_url, :comment
  # mount_uploader :image, ImageUploader

  before_save :check_url
  after_commit :process_async

  scope :recent, -> { order("created_at desc") }

  private

  def check_url
    self.image_processing = true if new_record? && temporary_image_url
  end

  def process_async
    ProcessImageWorker.perform_async(self.id) if temporary_image_url && image_processing
  end
end
