# == Schema Information
#
# Table name: groups
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_groups_on_user_id  (user_id)
#

class Group < ActiveRecord::Base
  belongs_to :owner, foreign_key: :user_id, class_name: :User
  
  validates_presence_of :owner, :name
  
  has_many :memberships, dependent: :destroy
  has_many :users, through: :memberships
  has_many :group_packages, -> { order(position: :asc) }, dependent: :destroy
  has_many :packages, through: :group_packages

  accepts_nested_attributes_for :memberships, :group_packages, allow_destroy: true

  def to_s
    self.name
  end
end
