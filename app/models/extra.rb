# == Schema Information
#
# Table name: extras
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  price       :float
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Extra < ActiveRecord::Base
  has_many :package_extras, dependent: :destroy
  has_many :packages, through: :package_extras

  validates_presence_of :name, :price

  def to_s
    self.name
  end
end
