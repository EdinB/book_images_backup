# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  invitation_token       :string
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_id          :integer
#  invited_by_type        :string
#  invitations_count      :integer          default("0")
#  roles                  :string           default("{}"), is an Array
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_invitation_token      (invitation_token) UNIQUE
#  index_users_on_invitations_count     (invitations_count)
#  index_users_on_invited_by_id         (invited_by_id)
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable, :invitable, :invite_for => 1.week
  before_create :build_default_profile
  
  has_many :owned_groups, foreign_key: :user_id, class_name: :Group
  has_one :profile, dependent: :destroy
  has_many :memberships, dependent: :destroy
  has_many :groups, through: :memberships
  has_many :orders, dependent: :destroy
  has_many :comments, dependent: :destroy
  delegate :first_name, :last_name, :shop, :shop_no, :address, :telephone, to: :profile, allow_nil: true

  ROLES = [
    'moderator'
  ]

  def self.admin_email
    ENV['ADMIN_EMAIL'] || "info@decento.dk"
  end
  
  def full_name
    "#{self.first_name} #{self.last_name}"
  end
  
  def comment_name
    name = full_name.empty? ? email : full_name
    if moderator?
      name = name + " <span class='label label-info'>ADMIN</span>"
    end
    name.html_safe
  end

  def all_group_ids
    # all_group_ids = (self.group_ids | self.owned_group_ids)
    all_group_ids = (self.group_ids)
    if all_group_ids.present?
      return all_group_ids
    else
      return [0]
    end
  end

  def moderator?
    roles.include? 'moderator'
  end
  
  def packages
    # packages = Package.arel_table
    # leam = Service.joins(:places).where(places[:place_name].matches('%leam%')
    # wark = Service.joins(:places).where(places[:place_name].matches('%wark%')
    # leam.where(id: wark.pluck(:id).uniq)
    Package.select('DISTINCT(packages.id), packages.name, packages.price, group_packages.position').joins(:groups).where("groups.id IN (#{all_group_ids.join(",")})")
  end

  def to_s
    self.email
  end

  def email_allowed?
    self.profile.send_email ? true : false
  end
    
  private
    def build_default_profile
      build_profile
      true
    end
end
