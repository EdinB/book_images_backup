# == Schema Information
#
# Table name: group_packages
#
#  id         :integer          not null, primary key
#  group_id   :integer
#  package_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  position   :integer
#
# Indexes
#
#  index_group_packages_on_group_id    (group_id)
#  index_group_packages_on_package_id  (package_id)
#

class GroupPackage < ActiveRecord::Base
  belongs_to :group
  belongs_to :package
  acts_as_list scope: :group
end
