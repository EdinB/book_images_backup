# == Schema Information
#
# Table name: orders
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  name                :string
#  phone               :string
#  phone2              :string
#  address             :string
#  city                :string
#  case_number         :string
#  notes               :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  state               :string
#  kind                :string
#  email               :string
#  square_meters       :integer
#  total_levels        :string
#  delivery_time       :datetime
#  attachments         :json
#  admin_attachments   :json
#  temporary_image_url :string
#
# Indexes
#
#  index_orders_on_user_id  (user_id)
#

class Order < ActiveRecord::Base
  include Sortable
  include PgSearch
  pg_search_scope :search_all, :against => [:name, :phone, :phone2, :address, :city, :case_number, :comments, :state, :kind, :email, :square_meters, :total_levels], :using => { :tsearch => {:prefix => true} }, :order_within_rank => "orders.updated_at DESC"
  mount_uploaders :attachments, AttachmentUploader
  acts_as_shopping_cart_using :order_item
  acts_as_commentable
  
  validates :name, :phone, :address, :city, :case_number, presence: true
  validates :square_meters, numericality: { greater_than_or_equal_to: 1 }
  validate :package_square_meters
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, allow_blank: true
  
  ORDER_TYPES = %w(Lejlighed Rækkehus Villa Sommerhus Erhverv Grund Andelsbolig Landbrug Andet)

  belongs_to :user
  has_many :order_admin_attachments
  accepts_nested_attributes_for :order_admin_attachments, allow_destroy: true
  after_update :check_if_delivery_time_set, :check_if_admin_uploaded_images
  has_many :order_additions, dependent: :destroy
  scope :active, -> { where.not(state: "in_progress")}
  scope :ordered, -> { where(state: "ordered") }
  scope :booked, -> { where(state: "booked") }
  scope :completed, -> { where(state: "completed") }
  scope :default_sort, -> (column, direction) {order(column + " " + direction)}
  scope :search_sort, -> (column, direction) {reorder(column + " " + direction)}

  state_machine :state, :initial => :in_progress do
    after_transition any => :ordered, do: :send_ordered_email
    after_transition any => :booked, do: :send_booked_email
    after_transition any => :completed, do: :send_completed_email
    
    event :placed_by_customer do
      transition :in_progress => :ordered
    end

    event :delivery_time_set do
      transition :ordered => :booked
    end

    event :complete do
      transition :booked => :completed
    end

    event :force_complete do
      transition all => :completed
    end

  end

  def tax_pct
    25
  end

  def user_label_class
    if self.ordered? 
      return "label-danger"
    elsif self.booked?
      return "label-warning"
    elsif self.completed?
      return "label-success"
    end
  end

  def show_file_uploader?
    if package.allow_upload == true
      return true
    end
    return false
  end

  def danish_state_name
    case human_state_name.camelcase
    when "Ordered"
      "Bestilte"
    when "Booked"
      "Bookede"
    when "Completed"
      "Færdig"
    end
  end

  def apply_square_meters_price_adjustment
    if self.in_progress?
      quantity = ((self.square_meters-1).to_f/300).floor
      if quantity > 0
        extra_amount = 150
        order_addition = self.order_additions.create(note: "Antal kvm pris justering", price: extra_amount, quantity: quantity)
        self.add(order_addition, order_addition.price, quantity)
      end
    end
  end

  def package_square_meters
    if package.max_80_square_meters == true && self.square_meters > 80
      errors.add(:square_meters, "Antal kvm være maksimalt 80 for den valgte pakke")
    end
    return true
  end

  def package
    self.shopping_cart_items.packages.first.package
  end

  private
    def send_ordered_email
      OrderUserMailer.ordered(self.id, self.user_id).deliver_later
      OrderAdminMailer.ordered(self.id, self.user_id).deliver_later
    end

    def send_booked_email
      OrderUserMailer.booked(self.id, self.user_id).deliver_later
    end

    def send_completed_email
      OrderUserMailer.completed(self.id, self.user_id).deliver_later
    end

    def check_if_delivery_time_set
      if self.ordered? && self.delivery_time.present?
        self.delivery_time_set
      end
    end

    def check_if_admin_uploaded_images
      if self.booked? && self.order_admin_attachments.count > 0
        self.complete
      end
    end
end
