# == Schema Information
#
# Table name: package_extras
#
#  id         :integer          not null, primary key
#  package_id :integer
#  extra_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_package_extras_on_extra_id    (extra_id)
#  index_package_extras_on_package_id  (package_id)
#

class PackageExtra < ActiveRecord::Base
  belongs_to :package
  belongs_to :extra
end
