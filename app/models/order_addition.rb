# == Schema Information
#
# Table name: order_additions
#
#  id         :integer          not null, primary key
#  order_id   :integer
#  note       :text
#  price      :integer
#  quantity   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_order_additions_on_order_id  (order_id)
#

class OrderAddition < ActiveRecord::Base
  belongs_to :order
end
