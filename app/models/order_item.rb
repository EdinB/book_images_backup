# == Schema Information
#
# Table name: order_items
#
#  id         :integer          not null, primary key
#  owner_id   :integer
#  owner_type :string
#  quantity   :integer
#  item_id    :integer
#  item_type  :string
#  price      :float
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class OrderItem < ActiveRecord::Base
  acts_as_shopping_cart_item_for :order

  belongs_to :package, foreign_key: :item_id, class_name: "Package"
  
  scope :packages, -> { where(item_type: "Package") }
  scope :extras, -> { where(item_type: "Extra") }
  scope :additions, -> { where(item_type: "OrderAddition") }

  def is_package?
    self.item_type == "Package"
  end

  def is_extra?
    self.item_type == "Extra"
  end

  def is_addition?
    self.item_type == "OrderAddition"
  end

  def extras
    if self.is_package?
      self.owner.shopping_cart_items.extras.where(item_id: self.item.extra_ids)
    end
  end
end
