class OrderPolicy
  attr_reader :user, :order

  def initialize(user, order)
    @user = user
    @order = order
  end

  def new?
    @order.user_id == @user.id
  end

  def show?
    @order.user_id == @user.id || @user.moderator?
  end

  def update?
    (@order.ordered? || @order.in_progress?) && @order.user_id == @user.id
  end

  def edit?
    @order.ordered? && @order.user_id == @user.id
  end

  def destroy?
    @order.ordered? && @order.user_id == @user.id
  end

end