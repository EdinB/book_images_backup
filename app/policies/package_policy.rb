class PackagePolicy
  attr_reader :user, :package

  def initialize(user, package)
    @user = user
    @package = package
  end

  def show?
    (@package.group_ids & @user.all_group_ids).present?
  end

end