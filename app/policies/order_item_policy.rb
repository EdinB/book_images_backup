class OrderItemPolicy
  attr_reader :user, :order_item

  def initialize(user, order_item)
    @user = user
    @order_item = order_item
  end

  def destroy?
    order = @order_item.owner
    order.user_id == @user.id && order.in_progress?
  end

end