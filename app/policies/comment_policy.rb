class CommentPolicy < ApplicationPolicy
  attr_reader :user, :comment

  def initialize(user, comment)
    @user = user
    @comment = comment
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      # resolving a scope https://github.com/elabs/pundit#scopes
      scope.all
    end
  end

  def destroy?
    @comment.user_id == @user.id || @user.moderator?
  end

  def create?
    @comment.commentable.user_id == user.id || @user.moderator?
  end

end