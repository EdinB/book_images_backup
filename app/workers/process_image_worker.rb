class ProcessImageWorker
 include Sidekiq::Worker

  def perform(photo_id)
    OrderAdminAttachment.find(photo_id).tap do |photo| # 1
      photo.remote_image_url = photo.temporary_image_url      # 2
      photo.image_processing = false      # 3
      photo.save!                       # 4
    end
  end
end