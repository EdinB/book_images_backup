ActiveAdmin.register Comment, as: "All Comments" do
  menu priority: 9
  menu :label => "Comments"
  actions :all, except: [:new]
  permit_params :_id, :user_id, :title, :comment, :commentable_id, :commentable_type
  
  
  filter :commentable_of_Order_type_case_number_cont, as: :string, label: 'Order Case Number Contains'
  filter :commentable_of_Order_type_name_cont, as: :string, label: 'Order Name Contains'
  filter :comment_cont, as: :string, label: 'Comment Contains'
  filter :user_profile_first_name_cont, as: :string, label: 'Author\'s First name Contains'
  filter :user_profile_last_name_cont, as: :string, label: 'Author\'s Last name Contains'
  filter :created_at


  batch_action :delete do |ids|
    Comment.find(ids).each do |comment|
      comment.destroy
    end
    redirect_to collection_path, alert: "Comments have been deleted."
  end

  action_item :only => :show do
    if resource.commentable_type == 'Order'
      model_obj = resource.commentable
      comm_path = send("#{resource.commentable_type.underscore}_path", model_obj)
      link_to 'Link to Order', "#{comm_path}", target: '_blank'
    end
  end

  form do |f|
    inputs 'Details' do      
      li "Created at: #{f.object.created_at}"
      li "By: #{f.object.user.comment_name}"
      input :comment
    end
    actions
  end


  index do
    selectable_column
    column "Content" do |comment|
      truncate(comment.comment, length: 25, omission: '...')
    end
    column :user, sortable: 'user_id'
    column "Order", :commentable
    column "Posted", sortable: 'created_at' do |comment|
      "#{time_ago_in_words(comment.created_at.in_time_zone("CET"))} ago"
    end
    actions do |comment|
    end
  end

  show :title => proc{|comment| "Comment ##{comment.id}, #{comment.comment.truncate(20)}" } do
    attributes_table do
      row :comment
      row :commentable
      row :commentable_type
      row :user_id
      row :created_at
      row :updated_at
    end
    # columns do
    #   column do
    #     if resource.commentable_type == 'Order'
    #       model_obj = resource.commentable
    #       comm_path = send("#{resource.commentable_type.underscore}_path", model_obj)
    #       preview_path = "#{comm_path}?scrollTo=comment-#{resource.id}"
    #       render 'write_a_comment_reply', preview_path: preview_path, comment: resource
    #     end
    #   end
    # end
  end


end