ActiveAdmin.register Group do
  menu priority: 5
  permit_params :id, :_id, :name, :user_id, :_destroy, memberships_attributes: [:user_id, :_destroy, :id, :_id], package_ids: []

  filter :owner
  filter :users
  filter :packages
  filter :name
  filter :created_at
  filter :updated_at
  
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs "User Details" do
      f.input :owner
      f.input :name
    end
    f.inputs do 
      f.input :packages, as: :check_boxes
    end
    f.inputs do
      f.has_many :memberships, heading: 'Memberships', new_record: true do |a|
        a.input :user
        a.input :_destroy, :as=>:boolean, :required => false, :label=>'Remove'
      end
    end
    actions
  end

  sidebar "Group Packages Details", only: [:show, :edit] do
    ul do
      li link_to "Assign Positions to Packages", admin_group_group_packages_path(group)
    end
  end

  show do
    panel "Group Details" do
      attributes_table_for group do
        row :id
        row :name
        row :owner
        row :created_at
        row :updated_at
      end
    end

    panel "Members" do
      attributes_table_for group do
        row :users do |g|
          g.users.map{|u| "<a href='/admin/users/#{u.id}'>#{u.email}</a>"}.join("<br />").html_safe
        end 
      end
    end

    panel "Linked Packages" do
      attributes_table_for group do
        row :packages do |g|
          g.packages.map{|p| "<a href='/admin/packages/#{p.id}'>#{p.name}</a> "}.join("<br />").html_safe
        end 
      end
    end
  end

end
