ActiveAdmin.register GroupPackage do
  actions :all, except: [:new, :destroy, :show]
  belongs_to :group
  permit_params :_id, :group_id, :package_id, :position
  config.filters = false

  config.sort_order = 'position_asc'
  index do
    selectable_column
    column :position
    column :package
    column :updated_at
    actions
  end
  
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs "Group Package Details" do
      f.input :package, input_html: {disabled: true}
      f.input :group, input_html: {disabled: true}
      f.input :position
    end
    actions
  end

  controller do
    define_method :permitted_params do
      params.permit *active_admin_namespace.permitted_params, :employer_id, :group_id, :package_id, :id, :_id, group_package: [:position, :group_id, :package_id, :id, :_id]
    end
  end
end