ActiveAdmin.register Extra do
  menu priority: 7
  permit_params :id, :_id, :name, :price, :description

  filter :packages
  filter :name
  filter :description
  filter :price
  filter :created_at
  filter :updated_at
  show do
    panel "Package Extra Details" do
      attributes_table_for extra do
        row :id
        row :name
        row :description
        row :price do |extra|
          danish_currency(extra.price)
        end
        row :created_at
        row :updated_at
      end
    end

    panel "Linked Packages" do
      attributes_table_for extra do
        row :packages do |e|
          e.packages.map{|p| "<a href='/admin/packages/#{p.id}'>#{p.name}</a> "}.join("<br />").html_safe
        end 
      end
    end


  end
end
