ActiveAdmin.register Profile do
  menu false
  permit_params :_id, :id, :first_name, :last_name, :shop, :shop_no, :address, :telephone, :send_email
  actions :all, except: [:new, :destroy]

  index do
    selectable_column
    id_column
    column :user
    column :shop 
    column :shop_no 
    column :address 
    column :telephone
    column "Send Email" do |profile|
      if profile.send_email
        status_tag "Yes", :green
      else
        status_tag "No", :red
      end
    end
    column :updated_at
    actions
  end

  form do |f|
    f.inputs "Profile Details" do
      f.input :user, input_html: {disabled: true}
      f.input :send_email
      f.input :first_name
      f.input :last_name
      f.input :shop 
      f.input :shop_no 
      f.input :address 
      f.input :telephone
    end
    f.actions
  end
end
