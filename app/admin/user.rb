ActiveAdmin.register User do
  menu priority: 3, label: "Users"
  permit_params :email, :password, :password_confirmation, roles: []
  # has_one :profile
  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column "Role" do |user|
      status_tag user.roles.first, :orange if user.roles.present?
    end
    column :created_at
    column "Invite Accepted" do |user|
      user.invitation_accepted_at.present? ? "YES" : "-"
    end
    column "Profile" do |resource|
      link_to("View Profile", admin_profile_path(resource.profile))
    end
    column "Impersonate User" do |resource|
      link_to("Impersonate", impersonate_admin_user_path(resource))
    end
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at


  form do |f|
    f.inputs "User Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :roles, as: :select, :collection => User::ROLES.map { |role| [role, role] }
    end
    f.actions
  end

  show do
    panel "User details" do
      attributes_table_for user do
        row :id 
        row :email
        row :roles 
        row :encrypted_password 
        row :reset_password_token 
        row :reset_password_sent_at 
        row :remember_created_at 
        row :sign_in_count 
        row :current_sign_in_at 
        row :last_sign_in_at 
        row :current_sign_in_ip 
        row :last_sign_in_ip 
        row :created_at 
        row :updated_at 
        row :invitation_token 
        row :invitation_created_at 
        row :invitation_sent_at 
        row :invitation_accepted_at 
        row :invitation_limit 
        row :invited_by_id 
        row :invited_by_type 
        row :invitations_count
      end
    end

    panel "Owner" do
      attributes_table_for user do
        row :groups do |u|
          u.owned_groups.map{|g| "<a href='/admin/groups/#{g.id}'>#{g.name}</a>"}.join("<br />").html_safe
        end 
      end
    end

    panel "Member" do
      attributes_table_for user do
        row :groups do |u|
          u.groups.map{|g| "<a href='/admin/groups/#{g.id}'>#{g.name}</a>"}.join("<br />").html_safe
        end 
      end
    end

  end
  
  action_item do
    link_to 'Invite New User', new_invitations_admin_users_path
  end
  collection_action :new_invitations do
    @user = User.new
  end 

  collection_action :send_invitation, :method => :post do
    @user = User.invite!(params[:user], current_user)
    if @user.errors.empty?
      flash[:success] = "User has been successfully invited." 
      redirect_to admin_users_path
    else
      messages = @user.errors.full_messages.map { |msg| msg }.join
      flash[:error] = "Error: " + messages
      redirect_to new_invitations_admin_users_path
    end
  end

  member_action :impersonate, :method => :get do
    sign_in(:user, User.find(params[:id]))
    redirect_to root_path
  end

  action_item :only => :show do
    link_to('Impersonate', impersonate_admin_user_path(resource))
  end
  
  controller do
    def update
      params[:user][:roles] = [params[:user][:roles]]
      if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      end
      super
    end
  end

end
