ActiveAdmin.register Order do
  menu priority: 8
  actions :all, except: :new
  permit_params :_id, :user_id, :id, :name, :phone, :phone2, :state, :address, :city, :case_number, :notes, :email, :total_levels, :square_meters, :kind, :delivery_time, order_admin_attachments_attributes: [:id, :order_id, :image, :_destroy, :temporary_image_url]
  scope :ordered, -> { where(state: "ordered") }, default: true
  scope :booked, -> { where(state: "booked") }
  scope :completed, -> { where(state: "completed") }

  controller do 
    def scoped_collection                                                                                                                                                                                                                                                
      Order.where.not(state: "in_progress")
    end
  end

  filter :user, collection: proc { User.joins(:orders).where.not("orders.state = 'in_progress'")}
  filter :name, collection: proc { Order.where.not(state: "in_progress")}
  filter :phone, collection: proc { Order.where.not(state: "in_progress")}
  filter :phone2, collection: proc { Order.where.not(state: "in_progress")}
  filter :address, collection: proc { Order.where.not(state: "in_progress")}
  filter :city, collection: proc { Order.where.not(state: "in_progress")}
  filter :case_number, collection: proc { Order.where.not(state: "in_progress")}
  filter :notes, collection: proc { Order.where.not(state: "in_progress")}
  filter :created_at, collection: proc { Order.where.not(state: "in_progress")}
  filter :updated_at, collection: proc { Order.where.not(state: "in_progress")}

  index do
    selectable_column
    id_column
    column :case_number
    column :address
    column :city
    column :phone
    column "State" do |order|
      if order.ordered?
        status_tag order.human_state_name.camelcase, :red
      elsif order.booked?
        status_tag order.human_state_name.camelcase, :orange
      else
        status_tag order.human_state_name.camelcase, :green
      end
    end
    column :delivery_time do |order|
      order.delivery_time.present? ? order.delivery_time.strftime("%d/%m/%Y %H:%M") : "-"
    end
    column do |order|
      link_to "#{order.comments.all.count} Comments", admin_all_comments_path("q[commentable_id_eq]" => order.id, "q[commentable_type_eq]" => "Order")
    end
    actions
  end

  form html: {multipart: true} do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs "Order Details" do
      if f.object.new_record?
        f.input :user
        f.input :state, input_html: {value: "ordered"}, as: :hidden
      else
        f.input :user, input_html: {disabled: true}
      end
      f.input :name, required: true
      f.input :email
      f.input :phone, label: "Telephone", required: true
      f.input :phone2, label: "Telephone2"
      f.input :address, required: true
      f.input :city, required: true
      f.input :case_number, required: true
      f.input :total_levels, required: true
      f.input :square_meters, required: true
      f.input :kind, collection: Order::ORDER_TYPES, prompt: "Select Type", label: "Type", required: true
      unless f.object.new_record?
        f.input :delivery_time, as: :date_time_picker, datepicker_options: {day_of_week_start: 1}
      end
      f.input :notes
      f.inputs "Pictures" do
        f.input :order_admin_attachments, :as => :modal_upload, :resource => f.object, :association => "order_admin_attachments", :attribute => "image", :preview_image_size => :thumb, max_file_size: 50.megabytes
      end
    end
    actions
  end

  action_item :only => :show do
    model_obj = resource
    comm_path = send("order_path", model_obj)
    link_to 'Link to Order', "#{comm_path}", target: '_blank'
  end
  show :title => proc{|order| order.address } do  
    panel "Order Details" do
      attributes_table_for order do
        row :id
        row :user
        row "State" do |order|
          if order.ordered?
            status_tag order.human_state_name.camelcase, :red
          elsif order.booked?
            status_tag order.human_state_name.camelcase, :orange
          else
            status_tag order.human_state_name.camelcase, :green
          end
        end
        row :name
        row :email
        row :phone
        row :phone2
        row :address
        row :city
        row :case_number
        row :total_levels
        row :square_meters
        row "Type" do |order|
          order.kind
        end
        row :delivery_time do |order|
          order.delivery_time.present? ? order.delivery_time.strftime("%d/%m/%Y %H:%M") : "-"
        end
        row :notes
        row :updated_at
      end
    end

    panel "User attachments" do
      text_node "#{order.attachments.map.with_index(1) {|attachment, index| '<a href='+attachment.url+'>Attachment #'+index.to_s+'</a>'}.join(' | ')}".html_safe
    end

    if order.completed?
      panel "Admin attachments" do
        order.order_admin_attachments.each do |img|
          if img.image_processing
            text_node(image_tag('image-processing.gif', height: 120)+'&nbsp;&nbsp;'.html_safe)
          else 
            text_node(link_to(image_tag(img.image.thumb.url, height: 120), img.image.url, target: "_blank")+'&nbsp;&nbsp;'.html_safe)
          end
        end
      end
    end

    # div "Admin Attachments" do # <- Note the div
    #   semantic_form_for [:admin, resource], builder: ActiveAdmin::FormBuilder do |f|
    #     f.inputs "Admin Images" do
    #       f.input :admin_attachments, as: :file, input_html: { multiple: true }, :hint => (f.object.admin_attachments.present? ? f.object.admin_attachments.map{|i| raw(image_tag(i.url, height: "100")) } : content_tag(:span, "Please upload an image to mark order as completed"))
    #     end
    #     f.actions
    #   end
    # end

    panel "Invoice" do
      table_for order.shopping_cart_items do
        column { |cart_item| cart_item.is_addition? ? cart_item.item.note : "<a href='/admin/#{cart_item.is_package? ? 'packages' : 'extras'}/#{cart_item.item.id}'>#{cart_item.item.name}</a>".html_safe}
        column { |cart_item| "x#{cart_item.quantity}" }
        column { |cart_item| danish_currency(cart_item.price) }
        column { |cart_item| danish_currency(cart_item.subtotal) }
      end
      text_node "&nbsp;<b>SubTotal:&nbsp;&nbsp;#{danish_currency(order.subtotal)}</b><br/>".html_safe
      text_node "&nbsp;<b>Tax (#{order.tax_pct}%):&nbsp;&nbsp;#{danish_currency(order.taxes)}</b><br/>".html_safe
      text_node "&nbsp;<b>Total Amount:&nbsp;&nbsp;#{danish_currency(order.total)}</b>".html_safe
    end



  end
end
