ActiveAdmin.register Package do
  menu priority: 6
  permit_params :id, :_id, :name, :price, :description, :allow_upload, :max_80_square_meters, extra_ids: []

  filter :groups
  filter :extras
  filter :name
  filter :price
  filter :allow_upload
  filter :created_at
  filter :updated_at
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs "Package Details" do
      f.input :name
      f.input :description
      f.input :price
      f.input :allow_upload
      f.input :max_80_square_meters
    end
    f.inputs do
      f.input :extras, as: :check_boxes
    end
    actions
  end

  index do
    selectable_column
    id_column
    column :name
    column :price
    column :allow_upload
    column :max_80_square_meters
    column :updated_at
    column :created_at
    actions
  end

  show do
    panel "Package Details" do
      attributes_table_for package do
        row :id
        row :name
        row :description
        row :price do |package|
          danish_currency(package.price)
        end
        row :allow_upload
        row :max_80_square_meters
        row :created_at
        row :updated_at
      end
    end

    panel "Linked Extras" do
      attributes_table_for package do
        row :extras do |p|
          p.extras.map{|e| "<a href='/admin/extras/#{e.id}'>#{e.name}</a> "}.join("<br />").html_safe
        end 
      end
    end

    panel "Linked Groups" do
      attributes_table_for package do
        row :groups do |p|
          p.groups.map{|g| "<a href='/admin/groups/#{g.id}'>#{g.name}</a> "}.join("<br />").html_safe
        end 
      end
    end


  end
end
