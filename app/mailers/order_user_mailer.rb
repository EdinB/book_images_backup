class OrderUserMailer < ApplicationMailer

  def ordered order_id, user_id
    @user = User.find(user_id)
    if @user.email_allowed?
      @order = Order.find(order_id)
      mail(to: @user.email, subject: "[DECENTO] Bestilling gennemført")
    end
  end

  def booked order_id, user_id
    @user = User.find(user_id)
    if @user.email_allowed?
      @order = Order.find(order_id)
      mail(to: @user.email, subject: "[DECENTO] Ordre “'#{@order.address}'” er blevet booket")
    end
  end

  def completed order_id, user_id
    @user = User.find(user_id)
    if @user.email_allowed?
      @order = Order.find(order_id)
      mail(to: @user.email, subject: "[DECENTO] Ordre “'#{@order.address}'” er nu færdigbehandlet")
    end
  end

end
