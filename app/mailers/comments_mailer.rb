class CommentsMailer < ApplicationMailer

  def new_comment comment_id, order_id, user_id
    @order = Order.find(order_id)
    @user = User.find(user_id)
    @comment = Comment.find(comment_id)
    if @user.moderator?
      mail(to: @order.user.email, subject: "[DECENTO] Ny kommentar fra Admin på ordre [#{@order.address}]")
    else
      mail(to: User.admin_email, subject: "[DECENTO] Ny kommentar fra #{@user.comment_name} på ordre [#{@order.address}]")
    end
  end

end
