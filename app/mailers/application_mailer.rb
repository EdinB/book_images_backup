require 'digest/sha2'

class ApplicationMailer < ActionMailer::Base
  add_template_helper(CurrencyHelper)
  default from: "DECENTO <#{User.admin_email}>", "Message-ID"=>"#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@#{ENV['domain_name']}"
  layout 'mailer'
end
