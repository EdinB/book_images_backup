class OrderAdminMailer < ApplicationMailer

  def ordered order_id, user_id
    @order = Order.find(order_id)
    @user = User.find(user_id)
    mail(to: User.admin_email, subject: "[DECENTO] Ny bestilling fra #{@user.groups.first.try(:name)}")
  end

end
