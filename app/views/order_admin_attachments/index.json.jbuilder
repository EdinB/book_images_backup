json.array!(@order_admin_attachments) do |order_admin_attachment|
  json.extract! order_admin_attachment, :id, :order_id, :image
  json.url order_admin_attachment_url(order_admin_attachment, format: :json)
end
