class OrderItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_order_item, only: [:destroy]
  before_action :authorize_order_item, only: [:destroy]
  before_action :set_order, only: [:destroy]
  
  def destroy
    @order.remove(@order_item.item, 1)
    if @order_item.is_package?
      @order_item.extras.each do |order_item_extra|
        @order.remove(order_item_extra.item, 1)
      end
    end
    if @order.empty?
      flash[:notice] = 'Alle Elementer fjernet fra ordre.'
      render js: "window.location = '#{packages_path}'"
    end
  end

  private
    def set_order
      @order = @order_item.owner
    end

    def set_order_item
      @order_item = OrderItem.find(params[:id])  
    end

    def authorize_order_item
      authorize @order_item
    end
end
