class OrderAdminAttachmentsController < InheritedResources::Base

  private

    def order_admin_attachment_params
      params.require(:order_admin_attachment).permit(:order_id, :image)
    end
end

