class CommentsController < ApplicationController
  before_filter :authenticate_user!, except: [:admin_reply]
  before_filter :get_commentable, except: [:destroy]
  skip_after_action :verify_authorized, only: [:admin_reply]
  
  def create
    @comment = @commentable.comments.build(comment_params)
    @comment.user = current_user
    authorize @comment, :create?
    if @comment.save
      CommentsMailer.new_comment(@comment.id, @comment.commentable_id, @comment.user_id).deliver_later
    else
      redirect_to @comment.commentable, notice: "Der opstod et problem i at skabe kommentar."
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @commentable = @comment.commentable
    if @comment.destroy
    else
      redirect_to @commentable, notice: "Der var et problem i at slette kommentar."
    end
  end

  private
    def comment_params
      params.require(:comment).permit(:id, :comment)
    end

    def get_commentable
      @commentable = Order.find(params[:order_id])
    end

end
