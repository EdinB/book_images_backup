class ApplicationController < ActionController::Base
  before_filter :set_locale
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include Pundit
  protect_from_forgery with: :exception
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  helper_method :current_order

  def current_order
    if !session[:order_id].nil?
      Order.find_by(id: session[:order_id], state: "in_progress")
    else
      order = current_user.orders.new
      order.save(validate: false)
      order
    end
  end

  def set_locale
    if self.kind_of?(ActiveAdmin::Devise::SessionsController) || self.kind_of?(ActiveAdmin::BaseController)
      I18n.locale = :en #or better: default_locale
    else
      I18n.locale = :da
    end
  end


  private

    def user_not_authorized
      flash[:alert] = "Du er ikke autoriseret til at udføre denne handling."
      redirect_to(request.referrer || root_path)
    end
end
