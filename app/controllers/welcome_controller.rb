require 'open-uri'

class WelcomeController < ApplicationController
   # enable streaming responses
  include ActionController::Streaming
  # enable zipline
  include Zipline

  before_action :authenticate_user!
  helper_method :sort_column, :sort_direction

  def index
    case params[:state]
    when "ordered"
      if params[:q].present?
        @orders = current_user.orders.ordered.search_all(params[:q]).custom_sort(sort_column, sort_direction).page(params[:page]).per(10)
      else
        @orders = current_user.orders.ordered.custom_sort(sort_column, sort_direction).page(params[:page]).per(10)
      end
    when "booked"
      if params[:q].present?
        @orders = current_user.orders.booked.search_all(params[:q]).custom_sort(sort_column, sort_direction).page(params[:page]).per(10)
      else
        @orders = current_user.orders.booked.custom_sort(sort_column, sort_direction).page(params[:page]).per(10)
      end
    when "completed"
      if params[:q].present?
        @orders = current_user.orders.completed.search_all(params[:q]).custom_sort(sort_column, sort_direction).page(params[:page]).per(10)
      else
        @orders = current_user.orders.completed.custom_sort(sort_column, sort_direction).page(params[:page]).per(10)
      end
    else
      if params[:q].present?
        @orders = current_user.orders.ordered.search_all(params[:q]).custom_sort(sort_column, sort_direction).page(params[:page]).per(10)
      else
        @orders = current_user.orders.ordered.custom_sort(sort_column, sort_direction).page(params[:page]).per(10)
      end
    end
  end

  def download_photos_as_zip
    if params[:image_ids].nil? || params[:image_ids].empty?
      redirect_to :back, notice: "Vælg mindst ét billede." 
    else
      file_urls = OrderAdminAttachment.where(id: params[:image_ids]).map{|i| i.image_url}

      file_mappings = file_urls.lazy.map { |url| [open(url), url.split('?')[0].split('/').last] }
      zipline(file_mappings, 'download.zip')
    end
  end
  private

    def sort_column
      Order.column_names.include?(params[:sort]) ? params[:sort] : "id"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
    end
end
