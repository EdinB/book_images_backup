# == Schema Information
#
# Table name: orders
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  name        :string
#  phone       :string
#  phone2      :string
#  address     :string
#  city        :string
#  case_number :string
#  comments    :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class OrdersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_order, only: [:new, :update]
  before_action :authorize_order, only: [:new, :update]
  helper_method :sort_column, :sort_direction
  
  def new
    @order.clear
    session[:order_id] = @order.id
    if params[:package_id].present?
      package = Package.find_by(id: params[:package_id])
      @order.add(package, package.price) if package.present?
    end
    if params[:extra_ids].present?
      Extra.where(id: params[:extra_ids]).each do |extra|
        @order.add(extra, extra.price)
      end
    end
  end
  
  def show
    @order = Order.find(params[:id])
    authorize @order
  end
  
  def update
    @order = Order.find(params[:id])
    authorize @order
    respond_to do |format|
      if @order.update(order_params)
        if @order.in_progress?
          @order.apply_square_meters_price_adjustment
          @order.placed_by_customer 
          session.delete(:order_id)
          format.html { redirect_to root_path, notice: 'Din bestilling er nu gennemført' }
        elsif @order.ordered?
          format.html { redirect_to @order, notice: 'Bestil er opdateret.' }
        end
      else
        if @order.in_progress?
          format.html { render :new }
        elsif @order.ordered?
          format.html { render :edit }
        end
      end
    end
  end

  def edit
    @order = Order.find(params[:id])
    authorize @order
  end

  def destroy
    @order = Order.find(params[:id])
    authorize @order
    if @order.destroy
      redirect_to root_path, notice: 'Ordren er nu slettet'
    end
  end

  def search
    @orders = current_user.orders.active.search_all(params[:q]).custom_sort(sort_column, sort_direction, "search").page(params[:page]).per(10)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = current_order
    end

    def order_params
      params.require(:order).permit(:_id, :id, :user_id, :name, :phone, :phone2, :address, :city, :case_number, :notes, :kind, :email, :square_meters, :total_levels, {attachments: []})
    end

    def authorize_order
      authorize @order
    end

    def sort_column
      Order.column_names.include?(params[:sort]) ? params[:sort] : "id"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
    end
  
end
