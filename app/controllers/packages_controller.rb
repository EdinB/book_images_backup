# == Schema Information
#
# Table name: packages
#
#  id           :integer          not null, primary key
#  name         :string
#  price        :float
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  descripton   :text
#  allow_upload :boolean
#

class PackagesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_package, only: [:show, :update]
  before_action :authorize_package, only: [:show, :update]
  helper_method :sort_column, :sort_direction

  def index
    @packages = current_user.packages.custom_sort(sort_column, sort_direction).page(params[:page]).per(30)
  end

  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_package
      @package = Package.find(params[:id])
    end

    def package_params
      params.require(:package).permit(:_id, :id, :first_name, :last_name, :shop, :shop_no, :address, :telephone)
    end

    def authorize_package
      authorize @package
    end

    def sort_column
      Package.column_names.include?(params[:sort]) ? params[:sort] : "group_packages.position"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end
end
