// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require turbolinks
//= require_tree .

function ready() {
  $('#select-all-order-photos').on('click', function (){
    $('#order_photos_section').find("input:checkbox").prop('checked',true);
  });
  $('#select-none-order-photos').on('click', function (){
    $('#order_photos_section').find("input:checkbox").prop('checked',false);
  });

  function toggleChevron(e) {
    $(e.target)
      .prev('.panel-heading')
      .find("i.indicator")
      .toggleClass('fa-chevron-down fa-chevron-up');
  }
  $('#accordion').on('hidden.bs.collapse', toggleChevron);
  $('#accordion').on('shown.bs.collapse', toggleChevron);

  $('input#order_square_meters').keyup(function(){
      var a = $(this).val();
      var cost = Math.floor((parseInt(a) - 1)/300) * 150
      if (cost > 0)
        $('#square_meters_extra_cost').text("Tillæg for ekstra opmåling: "+cost+" DKK"); 
      else
        $('#square_meters_extra_cost').text("Tillæg for ekstra opmåling: 0 DKK");
  });
}
$(document).ready(ready);
$(document).on('page:load', ready);