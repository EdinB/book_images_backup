S3DirectUpload.config do |c|
  c.access_key_id = ENV['AWS_ACCESS_KEY_ID']           # your access key id
  c.secret_access_key = ENV["AWS_SECRET_ACCESS_KEY"]   # your secret access key
  c.bucket = ENV['S3_BUCKET_NAME']                      # your bucket name
  c.region = ENV['AWS_REGION']
  c.url = "https://#{ENV['S3_BUCKET_NAME']}.s3.amazonaws.com"
end
