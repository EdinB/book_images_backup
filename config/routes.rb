require 'sidekiq/web'
Rails.application.routes.draw do

  require 'sidekiq/web'
  resources :order_admin_attachments
  root 'welcome#index'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  authenticate :admin_user do 
    mount Sidekiq::Web => '/sidekiq'
  end
  concern :commentable do
    resources :comments, shallow: true
  end
  get 'packages/index'
  resources :profiles, only: [:show, :edit, :update]
  resources :orders, only: [:new, :update, :show, :edit, :destroy], concerns: :commentable do
    collection do
      get :search
      post 'download_photos' => 'welcome#download_photos_as_zip'
    end
    resources :user_attachments, :only => [:create, :destroy] # support #create and #destroy
    resources :admin_attachments
  end
  resources :packages, only: [:index, :show]
  resources :order_items, only: [:destroy]
  resources :comments, only: [:create, :destroy]

  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
